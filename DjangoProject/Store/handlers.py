from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def pagination_handler(request, model_object, page_count=3):
    paginator = Paginator(model_object, page_count)
    page_count = request.GET.get('page')
    try:
        product_limit = paginator.page(page_count)
    except PageNotAnInteger:
        product_limit = paginator.page(1)
    except EmptyPage:
        product_limit = paginator.page(paginator.num_pages)
    return product_limit
