from django.urls import path, re_path
from . import views
from django.views.generic import TemplateView


app_name = 'Store'
urlpatterns = [
    path('', TemplateView.as_view(template_name='Basic/MainPage.html'), name='MainPage'),
    path('products/', views.ProductList, name='ProductList'),
    path('products/categories/<slug:category_slug>/',
         views.ProductList, name='products_with_category_slug'),
    path('detail/<int:product_id>/<slug:product_slug>/',
         views.ProductDetail, name='ProductDetail')
]
