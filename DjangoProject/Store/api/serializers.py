from rest_framework import serializers
from ..models import Product, Category


class ParendIdCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'slug')


class CategorySerializer(serializers.ModelSerializer):
    parent = ParendIdCategorySerializer(read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'parent', 'name', 'slug')


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
