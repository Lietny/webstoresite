from rest_framework import generics
from ..models import Product, Category
from . import serializers


class ProductViewList(generics.ListAPIView):
    queryset = Product.objects.filter(available=True)
    serializer_class = serializers.ProductSerializer


class ProductViewObject(generics.RetrieveAPIView):
    queryset = Product.objects.filter(available=True)
    serializer_class = serializers.ProductSerializer


class CategoryViewList(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer


class CategoryViewObject(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer
