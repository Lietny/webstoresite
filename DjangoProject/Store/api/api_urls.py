from django.urls import path
from . import api_views


app_name = 'api'
urlpatterns = [
    path('products/', api_views.ProductViewList.as_view(), name='ProductViewList'),
    path('products/<int:product_id>/', api_views.ProductViewObject.as_view(), name='ProductViewObject'),
    path('category/', api_views.CategoryViewList.as_view(), name='CategoryViewList'),
    path('category/<int:category_id>/', api_views.CategoryViewObject.as_view(), name='CategoryViewObject')
]

