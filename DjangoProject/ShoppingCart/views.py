from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_protect
from .forms import AddProductForm, OrderCreatingForm
from .ShoppingCart import Cart
from Store.models import Product
from .models import Order
from django.views.generic import View


# class add_to_cart(View):
#
#     def post(self, request, product_id):
#         requested_cart = Cart(request)
#         products = get_object_or_404(Product, pk=product_id)
#         add_product_form = AddProductForm(request.POST)
#         if add_product_form.is_valid():
#             form_cleaned_data = add_product_form.cleaned_data
#             requested_cart.add_product(product=products,
#                                        product_quantity=form_cleaned_data.get('quantity'),
#                                        update_quantity=form_cleaned_data.get('update'))

@require_POST
@csrf_protect
def add_to_cart(request, product_id):
    """
    pass the user request object to Cart class. Add product to shopping cart
    :param request: type => HttpRequest(). user request objects
    :param product_id: type => Model ORM. product id
    :return: type => HttpRedirect(). redirect to cart_detail() view
    """
    requested_cart = Cart(request)
    products = get_object_or_404(Product, pk=product_id)
    add_product_form = AddProductForm(request.POST)
    if add_product_form.is_valid():
        form_cleaned_data = add_product_form.cleaned_data
        requested_cart.add_product(product=products,
                                   product_quantity=form_cleaned_data.get('quantity'),
                                   update_quantity=form_cleaned_data.get('update'))
    return redirect('ShoppingCart:cart_detail')


def remove_from_cart(request, product_id):
    """
    pass the user request object to Cart class. Remove product from shopping cart
    :param request: type => HttpRequest(). user request objects
    :param product_id: type => Model ORM. product id
    :return: type => HttpRedirect(). redirect to cart_detail() view
    """
    requested_cart = Cart(request)
    product = get_object_or_404(Product, pk=product_id)
    requested_cart.remove_product(product)
    return redirect('ShoppingCart:cart_detail')


def cart_detail(request):
    """
    pass the user request object to Cart class
    :param request: type => HttpRequest(). user request objects
    :return: render: type => HttpResponse().
    """
    requested_cart = Cart(request)
    return render(request, 'ShoppingCart/Detail.html', context={'cart': requested_cart})


@csrf_protect
def order_create(request):
    requested_cart = Cart(request)
    if request.method == 'POST':
        order_creating_form = OrderCreatingForm(request.POST)
        if order_creating_form.is_valid():
            customer_save = order_creating_form.save()
            for cart_items in requested_cart:
                Order.objects.create(customer=customer_save, product=cart_items.get('product'),
                                     price=cart_items.get('price'), quantity=cart_items.get('quantity'))
            ordered_product = [value for value in customer_save.customer.all()]
            requested_cart.clear_cart_session()
            return render(request, 'ShoppingCart/OrderSuccessfully.html',
                          context={'order_successfully': customer_save, 'ordered_product': ordered_product})
    else:
        order_creating_form = OrderCreatingForm()
        return render(request, 'ShoppingCart/OrderRegistration.html',
                      context={'requested_cart': requested_cart, 'order_creating_form': order_creating_form})

