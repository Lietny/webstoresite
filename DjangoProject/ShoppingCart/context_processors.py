from .ShoppingCart import Cart


def shopping_cart(request):
    return {'shopping_cart_processor': Cart(request)}
